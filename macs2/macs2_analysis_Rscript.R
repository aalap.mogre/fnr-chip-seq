# MACS2-2.1.1.20160309
# Load files
flagFiles <- list.files("../alignments/", pattern = "^flag.*.sorted.bam$")
flagFiles2 <- gsub(".sorted.bam", "", flagFiles)
inputFiles <- list.files("../alignments/", pattern = "^input.*.sorted.bam$")
inputFiles2 <- gsub(".sorted.bam", "", inputFiles)
mockFiles <- list.files("../alignments/", pattern = "^mock.*.sorted.bam$")
mockfiles2 <- gsub(".sorted.bam", "", mockFiles)

# Call narrow peaks
# also can try option -q 1e-15
# can also try --call-summits: but you'll get loads of subpeaks which are not very useful in this situation.
system(paste0("macs2 callpeak -g 6.0e+6 -f BAMPE -m 1 500 --keep-dup all -q 1e-2", 
              " -n ", flagFiles2[1]," --outdir ", flagFiles2[1],
              " -t ../alignments/", flagFiles[1],
              " -c ../alignments/", mockFiles[1])) 
system(paste0("macs2 callpeak -g 6.0e+6 -f BAMPE -m 1 500 --keep-dup all -q 1e-10", 
              " -n ", flagFiles2[2]," --outdir ", flagFiles2[2],
              " -t ../alignments/", flagFiles[2],
              " -c ../alignments/", mockFiles[3])) 
system(paste0("macs2 callpeak -g 6.0e+6 -f BAMPE -m 1 500 --keep-dup all -q 1e-5", 
              " -n ", flagFiles2[3]," --outdir ", flagFiles2[3],
              " -t ../alignments/", flagFiles[3],
              " -c ../alignments/", mockFiles[3])) 

dir.create("all_narrow_peaks", showWarnings = F)

### cp relevant files to folders 
for (i in flagFiles2) {
  system(paste0("cp ", i, "/", i, "_peaks.narrowPeak all_narrow_peaks/"))
}

for (i in flagFiles2) {
  system(paste0("cd ", i, "&&  Rscript *model.r"))
}
