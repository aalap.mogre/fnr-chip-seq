library(data.table)
### Loading annotations ###
ann <- fread("../alignments/combined_annotation/combined_annotation.gff3")
colnames(ann) <- c("Sequence", "Source", "Feature", "Start", "End",
                   "Score", "Strand", "Frame", "Attributes")

grep2 <- function(pattern, x) {
  if (length(grep(pattern, x)) == 0) {
    return("NA")
  } else {
    return(grep(pattern, x))
  }
}

temp <- strsplit(ann$Attributes, split = ";")
temp2 <- NULL
for (i in 1:length(temp)) {
  temp3 <- temp[[i]]
  temp4 <- temp3[grep2("ID=", temp3)]
  temp5 <- temp3[grep2("Name=", temp3)]
  temp6 <- temp3[grep2("Alias=", temp3)]
  temp7 <- temp3[grep2("description=", temp3)]
  temp8 <- temp3[grep2("Note=", temp3)]
  temp9 <- c(temp4, temp5, temp6, temp7, temp8)
  temp2 <- rbind(temp2, temp9)
}
rownames(temp2) <- NULL
temp2 <- as.data.frame(temp2)
colnames(temp2) <- c("ID", "Name", "Alias", "description", "Note")
temp2$ID <- gsub("ID=", "", temp2$ID)
temp2$Name <- gsub("Name=", "", temp2$Name)
temp2$Alias <- gsub("Alias=", "", temp2$Alias)
temp2$description <- gsub("description=", "", temp2$description)
temp2$Note <- gsub("Note=", "", temp2$Note)

ann <- cbind(ann, temp2)

temp <- unique(ann$Sequence)
annPlusList <- list()
annMinusList <- list()
for (i in 1:length(temp)) {
  annPlusList[[i]] <- ann[(ann$Strand == "+") & (ann$Sequence == temp[i]), ]
  annMinusList[[i]] <- ann[(ann$Strand == "-") & (ann$Sequence == temp[i]), ]
}
names(annPlusList) <- names(annMinusList) <- temp

### Loading annotations done ###


### Function to annotate macs2 peaks ###
### Load annotation first! ###
annotatePeaks <- function(peaks) {
  # $V1 = chromosome; $V2 = start; $V3 = stop;
  windows <- cbind(peaks[,2]-100, peaks[,3]+100) # broaden the peak boundaries to search for genes
  annotatedPeaks <- peaks
  
  col1 <- rep(NA, nrow(peaks))
  col2 <- rep(NA, nrow(peaks))
  col3 <- rep(NA, nrow(peaks))
  for (i in 1:nrow(peaks)) {
    plusGenes <- annPlusList[[peaks[,1][i]]]
    minusGenes <- annMinusList[[peaks[,1][i]]]
    plusFound <- plusGenes[(plusGenes$Start > windows[i,1]) & (plusGenes$Start < windows[i,2]), ]
    minusFound <- minusGenes[(minusGenes$End > windows[i,1]) & (minusGenes$End < windows[i,2]), ]
    found <- rbind(plusFound, minusFound)
    # If no peaks found, broaden search area further
    if (nrow(found) == 0) {
      plusFound <- plusGenes[(plusGenes$Start > (windows[i,1]-500)) & (plusGenes$Start < (windows[i,2]+500)), ] # broaden by another 500 bases, total = 600
      minusFound <- minusGenes[(minusGenes$End > (windows[i,1]-500)) & (minusGenes$End < (windows[i,2]+500)), ]
      found <- rbind(plusFound, minusFound)
    }
    # If no peaks found, the peaks might be internal to gene and not in favourable location relative to start end of gene
    # So find gene internal peaks
    if (nrow(found) == 0) {
      midpoint <- mean(windows[i,])
      plusFound <- plusGenes[(midpoint > plusGenes$Start) & (midpoint < plusGenes$End),]
      minusFound <- minusGenes[(midpoint > minusGenes$Start) & (midpoint < minusGenes$End),]
      found <- rbind(plusFound, minusFound)
    }
    # If no peaks found, the peaks might be between converging genes eg b/w STM3681 and selB
    # If absolute distance of peak midpoint is less than 100 bases from the start or end of a gene - report that gene
    if (nrow(found) == 0) {
      plusFound <- plusGenes[(abs(midpoint-plusGenes$Start) < 100)|(abs(midpoint-plusGenes$End) < 100),]
      minusFound <- minusGenes[(abs(midpoint-minusGenes$Start) < 100)|(abs(midpoint-minusGenes$End) < 100),]
      found <- rbind(plusFound, minusFound)
    }
    found <- found[order(found$Start), ]
    peakIds <- paste0(found$ID, collapse = ",")
    peakNames <- paste0(found$Name, collapse = ",") # this Name is gene name
    peakDescriptions <- paste0(found$description, collapse = ",")
    col1[i] <- peakIds
    col2[i] <- peakNames
    col3[i] <- peakDescriptions
  }
  col1[nchar(col1) == 0] <- "unannotated"
  col2[nchar(col2) == 0] <- "unannotated"
  col3[nchar(col3) == 0] <- "unannotated"
  annotatedPeaks$ids <- col1
  annotatedPeaks$genes <- col2
  annotatedPeaks$descriptions <- col3
  return(annotatedPeaks)
}
### function to annotate peaks done! ####